# Project SafetyNet
## Build
`mvn clean verify site`
## Report
Jacoco reports in ./coverage-report <br/>
Surefire/Failsafe report in ./target/site <br/>
Each module have its test report

