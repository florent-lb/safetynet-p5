package com.safetynet.alerts.contract;

import com.safetynet.alerts.entity.impl.FireStation;

import java.util.*;

/**
 * Manager for entities FireStation
 */
public interface FireStationManager {

    /**
     * Method for create a new Fire Station
     * @param station the station to add
     */
    void addFireStation(FireStation station) throws NullPointerException;

    /**
     * Method for update a FireStation (number only)
     * @param station
     * @throws NullPointerException if no station found
     */
    void updateNumber(FireStation station) throws NullPointerException;

    /**
     * For removed a station from the repository
     * @param station to remove
     */
    void deleteFireStation(FireStation station) ;

    /**
     * Return the cover of the station
     * @param number
     * @return Map with keys :
     * <ul>
     *     <li>people : array</li>
     *     <li>nb_adults : integer</li>
     *     <li>nb_child : integer</li>
     * </ul>
     */
    Map<String, Object> getCoverStation(Integer number);

    /**
     * Retun Map with key : Number of the station, Values : list of people information cover by the station number(key)
     * @param stationNumbers
     * @return ex: Entry(1, List.of( key : allergies|value : [shellfish] , key : phone | value : "841-874-6512"...etc))
     */
    Map<Integer, List<Map<String, Object>>> getCoverStation(List<Integer> stationNumbers);

    /**
     * Get list of phones of people cover by the station
     * @param number of the station
     * @return List of phone number
     */
    Set<String> getPhoneCoverFromStation(Integer number);

    /**
     * Searcg a firestation per its address
     * @param address of the station
     * @return potentially the station
     */
    Optional<FireStation> getStationByAddress(String address);


}
