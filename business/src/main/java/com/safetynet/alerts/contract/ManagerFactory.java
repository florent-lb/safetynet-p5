package com.safetynet.alerts.contract;

/**
 * Manager for access to the entity manager
 */
public interface ManagerFactory
{
    FireStationManager getFireStationManager();

    PersonManager getPersonManager();

    MedicalRecordManager getMedicalRecordManager();
}
