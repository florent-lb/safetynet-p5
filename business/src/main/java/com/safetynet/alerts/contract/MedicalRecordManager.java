package com.safetynet.alerts.contract;

import com.safetynet.alerts.entity.impl.MedicalRecord;
import com.safetynet.alerts.entity.impl.Person;

import java.util.List;
import java.util.Map;


/**
 * Manager for Medical Record entity
 */
public interface MedicalRecordManager {
    void addMedicalRecord(MedicalRecord medRecord);

    void updateMedicalRecord(MedicalRecord medRecord);

    void deleteMedicalRecord(String lastName, String firstName);

    /**
     * Return a map with person in key and his medical record on value
     * @param people people to map to their medical records
     * @return map, default implementation : {@link java.util.HashMap}
     */
    Map<Person, MedicalRecord> mapRecordToPerson(List<Person> people);
}
