package com.safetynet.alerts.contract;

import com.safetynet.alerts.entity.impl.MedicalRecord;
import com.safetynet.alerts.entity.impl.Person;

import java.util.List;
import java.util.Map;

public interface PersonManager {
    /**
     * Use for add a person in Person repository
     * @param person the person to add
     */
    void addPerson(Person person);
    /**
     * Use for update a person in Person repository
     * @param person the person to add
     */
    void updatePerson(Person person);
    /**
     * Use for delete a person in Person repository
     * @param lastName
     * @param firstName
     */
    void deletePerson(String lastName, String firstName);

    /**
     * Return the list of person who the station address is equals to the address in parameter
     * @param address address station search
     * @return Empty if no peaple found
     */
    List<Person> getPersonsByStationAddress(String address);

    /**
     * Return a map with address in key and children as value
     * @param address
     * @return
     */
    Map<String, List<Person>> getChildInformationByAddress(String address);
    /**
     * Return a List of map, each map his a person informations with medical record
     * @param address of people search
     * @return
     */
    List<Map<String, Object>> getPersonInformationsAtAddress(String address);

    List<Person> getChildren(Map<Person, MedicalRecord> mappingPersonRecord);

    List<Person> getAdults(Map<Person, MedicalRecord> mappingPersonRecord);

    /**
     * Return a map including person information & medicalRecordInformation
     * @param person
     * @param medicalRecord
     * @return
     */
    Map<String, Object> createMedicalPersonJson(Person person, MedicalRecord medicalRecord);

    /**
     * Return a map including all person information & medicalRecordInformation
     * @param person
     * @param medicalRecord
     * @return
     */
    Map<String, Object> createFullMedicalPersonJson(Person person, MedicalRecord medicalRecord);

    /**
     * Search all Person and their medical record with the same firstName
     * @param firstName
     * @param lastName
     * @return return Json object with key people and value list of people with the same first name
     */
    List<Map<String, Object>> getPersonInformations(String firstName, String lastName);

    /**
     * Search all email of people in city in parameter
     * @param city
     * @return Json object with all email
     */
    List<String> getPersonMailFromCity(String city);
}
