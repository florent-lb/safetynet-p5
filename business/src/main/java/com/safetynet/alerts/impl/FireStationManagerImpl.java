package com.safetynet.alerts.impl;

import com.safetynet.alerts.contract.FireStationManager;
import com.safetynet.alerts.contract.ManagerFactory;
import com.safetynet.alerts.entity.impl.FireStation;
import com.safetynet.alerts.entity.impl.MedicalRecord;
import com.safetynet.alerts.entity.impl.Person;
import com.safetynet.alerts.repository.contract.FireStationRepository;
import lombok.extern.log4j.Log4j2;

import javax.inject.Inject;
import javax.inject.Named;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

@Named
@Log4j2(topic = "FireStationManagerImpl")
public class FireStationManagerImpl implements FireStationManager {

    @Inject
    private FireStationRepository fireStationRepository;

    @Inject
    private ManagerFactory managerFactory;

    @Override
    public void addFireStation(FireStation station) throws NullPointerException {

        Objects.requireNonNull(station.getAddress(), "The address of the station is null");
        fireStationRepository.add(station);
    }

    @Override
    public void updateNumber(FireStation station) throws NullPointerException {
        Objects.requireNonNull(station, "The station is null");
        FireStation toUpdate = fireStationRepository.findStationByAddress(station.getAddress()).orElseThrow(() -> new NullPointerException("No station FireStation with address : " + station.getAddress()));
        toUpdate.setStation(station.getStation());
        logger.debug("Updated : " + station.toString());
    }

    @Override
    public void deleteFireStation(FireStation station) {

        fireStationRepository.deleteFireStation(station);
    }

    @Override
    public Map<String, Object> getCoverStation(Integer number) {

        Map<String,Object> keysReturn = new HashMap<>();

        List<Person> personsOnStations = peopleCoverByStation(number);

        Map<Person, MedicalRecord> records = managerFactory.getMedicalRecordManager().mapRecordToPerson(personsOnStations);

        long nbAdults = records.entrySet()
                .stream()
                .filter(entry ->
                        entry.getValue().getBirthdate().isBefore(LocalDate.now().minusYears(18))
                ).count();
        Long nbChild = personsOnStations.size() - nbAdults;


        keysReturn.put("people", personsOnStations);
        keysReturn.put("nb_adults", nbAdults);
        keysReturn.put("nb_child", nbChild);

        return keysReturn;
    }

    @Override
    public Map<Integer, List<Map<String, Object>>> getCoverStation(List<Integer> stationNumbers) {

        Map<Integer, List<Map<String, Object>>> stationCoverJson = new HashMap<>();
        stationNumbers.forEach(numStation ->
        {
            List<Map<String, Object>> peopleOnCover = new ArrayList<>();
            List<Person> personsOnStations = peopleCoverByStation(numStation);
            Map<Person, MedicalRecord> mapping = managerFactory.getMedicalRecordManager().mapRecordToPerson(personsOnStations);
            mapping.entrySet()
                    .stream()
                    .sorted(Map.Entry.comparingByKey())
                    .forEach(entry ->
                            peopleOnCover.add(managerFactory.getPersonManager().createMedicalPersonJson(entry.getKey(), entry.getValue()))

            );
            stationCoverJson.put(numStation, peopleOnCover);
        });

        return stationCoverJson;
    }

    private List<Person> peopleCoverByStation(int number) {
        List<Person> people = new ArrayList<>();
        fireStationRepository.findStationByStation(number)
                .forEach(station ->
                {
                    people.addAll(managerFactory.getPersonManager().getPersonsByStationAddress(station.getAddress()));
                });

        return people;
    }


    @Override
    public Set<String> getPhoneCoverFromStation(Integer number) {

        return peopleCoverByStation(number)
                .stream()
                .map(Person::getPhone)
                .collect(Collectors.toSet());
    }

    @Override
    public Optional<FireStation> getStationByAddress(String address) {

        return fireStationRepository.findStationByAddress(address);

    }
}
