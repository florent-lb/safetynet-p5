package com.safetynet.alerts.impl;

import com.safetynet.alerts.contract.FireStationManager;
import com.safetynet.alerts.contract.ManagerFactory;
import com.safetynet.alerts.contract.MedicalRecordManager;
import com.safetynet.alerts.contract.PersonManager;

import javax.inject.Inject;
import javax.inject.Named;

@Named
public class ManagerFactoryImpl implements ManagerFactory {

    @Inject
    private FireStationManager fireStationManager;

    @Inject
    private PersonManager personManager;

    @Inject
    private MedicalRecordManager medicalRecordManager;

    @Override
    public FireStationManager getFireStationManager() {
        return fireStationManager;
    }

    @Override
    public PersonManager getPersonManager() {
        return personManager;
    }

    @Override
    public MedicalRecordManager getMedicalRecordManager() {
        return medicalRecordManager;
    }


}
