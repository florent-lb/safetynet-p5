package com.safetynet.alerts.impl;

import com.safetynet.alerts.contract.MedicalRecordManager;
import com.safetynet.alerts.entity.impl.MedicalRecord;
import com.safetynet.alerts.entity.impl.Person;
import com.safetynet.alerts.repository.contract.MedicalRecordRepository;
import com.safetynet.alerts.repository.impl.MedicalRecordRepositoryImpl;
import lombok.extern.slf4j.Slf4j;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.HashMap;
import java.util.List;

@Named
@Slf4j
public class MedicalRecordManagerImpl implements MedicalRecordManager {


    @Inject
    private MedicalRecordRepository medicalRecordRepository;

    @PostConstruct
    public void setup() {

    }

    @Override
    public void addMedicalRecord(MedicalRecord medRecord) {
        medicalRecordRepository.add(medRecord);
    }

    @Override
    public void updateMedicalRecord(MedicalRecord medRecord) {
        medicalRecordRepository.updateMedicalReccord(medRecord);
        logger.debug("Updated : " + medRecord.toString());
    }

    @Override
    public void deleteMedicalRecord(String lastName, String firstName) {
        medicalRecordRepository.searchMedicalRecordByKey(lastName, firstName)
                .ifPresent(medicalRecordRepository::delete);
    }

    @Override
    public HashMap<Person, MedicalRecord> mapRecordToPerson(List<Person> people) {

        HashMap<Person, MedicalRecord> mapRecords = new HashMap<>(people.size());
        people.forEach(person ->
            medicalRecordRepository.searchMedicalRecordByKey(person.getLastName(),person.getFirstName())
                    .ifPresent(medicalRecord -> mapRecords.put(person,medicalRecord))
        );
        return mapRecords;
    }
}
