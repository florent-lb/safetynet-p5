package com.safetynet.alerts.impl;

import com.safetynet.alerts.contract.FireStationManager;
import com.safetynet.alerts.contract.MedicalRecordManager;
import com.safetynet.alerts.contract.PersonManager;
import com.safetynet.alerts.entity.impl.MedicalRecord;
import com.safetynet.alerts.entity.impl.Person;
import com.safetynet.alerts.repository.contract.PersonRepository;
import com.safetynet.alerts.repository.impl.PersonRepositoryImpl;
import lombok.extern.slf4j.Slf4j;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Named
@Slf4j
public class PersonManagerImpl implements PersonManager {

    @Inject
    private PersonRepository repository;

    @Inject
    private MedicalRecordManager medicalRecordManager;

    @Inject
    private FireStationManager fireStationManager;

    @PostConstruct
    public void setup() {
    }

    @Override
    public void addPerson(Person person) {
        repository.add(person);
    }

    @Override
    public void updatePerson(Person person) {
        repository.updatePerson(person);
        logger.debug("Updated : " + person.toString());
    }

    @Override
    public void deletePerson(String lastName, String firstName) {
        repository.findPerson(lastName, firstName)
                .ifPresent(repository::delete);
    }

    @Override
    public List<Person> getPersonsByStationAddress(String address) {

        return repository.searchByAddress(address);


    }

    @Override
    public Map<String, List<Person>> getChildInformationByAddress(String address) {

        List<Person> personToAddress = getPersonsByStationAddress(address);
        Map<Person, MedicalRecord> recordsMap = medicalRecordManager.mapRecordToPerson(personToAddress);

        List<Person> childToAddress = getChildren(recordsMap);
        List<Person> adultToAdress = getAdults(recordsMap);

        Map<String, List<Person>> retour = new HashMap<>();

        retour.put("children", childToAddress);
        retour.put("others", adultToAdress);
        return retour;
    }

    @Override
    public List<Map<String, Object>> getPersonInformationsAtAddress(String address) {

        List<Person> personToAddress = getPersonsByStationAddress(address);
        Map<Person, MedicalRecord> recordsMap = medicalRecordManager.mapRecordToPerson(personToAddress);
        List<Map<String, Object>> retour = new ArrayList<>(recordsMap.size());

        fireStationManager.getStationByAddress(address)
                .ifPresent(station ->
                {
                    recordsMap.forEach((person, medicalRecord) ->
                    {
                        Map<String, Object> keys = createMedicalPersonJson(person, medicalRecord);
                        keys.put("station", station.getStation());
                        retour.add(keys);
                    });
                });

        return retour;
    }


    @Override
    public List<Person> getChildren(Map<Person, MedicalRecord> mappingPersonRecord) {
        return mappingPersonRecord.entrySet()
                .stream()
                .filter(entry -> entry.getValue().getBirthdate().isAfter(LocalDate.now().minusYears(18)))
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());
    }

    @Override
    public List<Person> getAdults(Map<Person, MedicalRecord> mappingPersonRecord) {
        return mappingPersonRecord.entrySet()
                .stream()
                .filter(entry -> entry.getValue().getBirthdate().isBefore(LocalDate.now().minusYears(18)) || entry.getValue().getBirthdate().isEqual(LocalDate.now().minusYears(18)))
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());
    }

    @Override
    public Map<String, Object> createMedicalPersonJson(Person person, MedicalRecord medicalRecord) {
        HashMap<String, Object> personInformations = new HashMap<>();
        personInformations.put("name", person.getLastName());
        personInformations.put("phone", person.getPhone());
        personInformations.put("age", Period.between(medicalRecord.getBirthdate(), LocalDate.now()).getYears());
        personInformations.put("medications", medicalRecord.getMedications());
        personInformations.put("allergies", medicalRecord.getAllergies());
        return personInformations;
    }

    @Override
    public Map<String, Object> createFullMedicalPersonJson(Person person, MedicalRecord medicalRecord) {

        Map<String, Object> personJson = createMedicalPersonJson(person, medicalRecord);
        personJson.put("email", person.getEmail());
        personJson.put("address", person.getAddress());
        personJson.put("city", person.getCity());
        personJson.put("zip", person.getZip());

        return personJson;
    }


    @Override
    public List<Map<String, Object>> getPersonInformations(String firstName, String lastName) {

        Map<Person, MedicalRecord> peopleWithMedicalRecord = medicalRecordManager.mapRecordToPerson(repository.findPerson(lastName));

        return peopleWithMedicalRecord
                .entrySet()
                .stream()
                .map(personMedicalRecordEntry -> createFullMedicalPersonJson(personMedicalRecordEntry.getKey(), personMedicalRecordEntry.getValue()))
                .collect(Collectors.toList());
    }

    @Override
    public List<String> getPersonMailFromCity(String city) {


        return
                repository.searchByCity(city)
                        .stream()
                        .map(Person::getEmail)
                        .collect(Collectors.toList());

    }
}
