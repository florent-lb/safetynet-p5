package com.safetynet.alerts.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.safetynet.alerts.entity.impl.FireStation;
import com.safetynet.alerts.entity.impl.MedicalRecord;
import com.safetynet.alerts.entity.impl.Person;
import com.safetynet.alerts.repository.contract.FireStationRepository;
import com.safetynet.alerts.repository.contract.MedicalRecordRepository;
import com.safetynet.alerts.repository.contract.PersonRepository;
import lombok.extern.log4j.Log4j2;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import java.io.*;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Class used for initialize the repository with Json file
 */
@Named
@Singleton
public class RepositoryInitializer {

    private final static Logger logger = LoggerFactory.getLogger(RepositoryInitializer.class);
    
    @Inject
    public FireStationRepository fireStationRepository;

    @Inject
    public MedicalRecordRepository medicalRecordRepository;

    @Inject
    public PersonRepository personRepository;

    /**
     * Use for instantiate the repositories with data if json file is found
     */
    public void initializeRepositories(String pathToFile) {
        logger.debug("Initialize Repositories...");
        try {
            searchDataFile(pathToFile).ifPresent(inData ->
            {
                try {
                    ObjectMapper mapper = new JsonMapper();
                    JsonNode data = mapper.readTree(inData);

                    JsonNode firestationsNode = data.findValue("firestations");
                    JsonNode personsNode = data.findValue("persons");
                    JsonNode mdNodes = data.findValue("medicalrecords");

                    AtomicInteger index = new AtomicInteger();
                    firestationsNode.elements().forEachRemaining(firestationNode ->
                    {
                        index.incrementAndGet();
                        try {
                            FireStation station = mapper.readValue(firestationNode.toString(), FireStation.class);
                            fireStationRepository.add(station);
                        } catch (JsonProcessingException e) {
                            logger.error("Impossible to get Json data for FireStation", e);
                        }
                    });

                    personsNode.elements().forEachRemaining(personNode ->
                    {
                        try {
                            Person person = mapper.readValue(personNode.toString(), Person.class);
                            personRepository.add(person);
                        } catch (JsonProcessingException e) {
                            logger.error("Impossible to get Json data for Person", e);
                        }
                    });

                    mdNodes.elements().forEachRemaining(mdNode ->
                    {
                        try {
                            MedicalRecord record = mapper.readValue(mdNode.toString(), MedicalRecord.class);
                            medicalRecordRepository.add(record);
                        } catch (JsonProcessingException e) {
                            logger.error("Impossible to get Json data for MedicalRecord", e);
                        }
                    });
                } catch (IOException e) {
                    logger.error("Error when reading the Json file", e);
                }
            });

        } catch (FileNotFoundException e) {
            logger.error("Impossible to find data File", e);
        }
    }

    /**
     * used by {@link #initializeRepositories(String)} ()} for extract InputStream from the data.json file
     *
     * @return potentially a stream with json data
     * @throws FileNotFoundException if no file found at ${basedir}/data/data.json or src/main/resources/data/data.json
     */
    private static Optional<InputStream> searchDataFile(String filePath) throws FileNotFoundException {

        File file = new File(filePath);
        if (!file.exists()) {
            return Optional.ofNullable(RepositoryInitializer.class.getClassLoader().getResourceAsStream("data/data.json"));
        } else {
            return Optional.of(new FileInputStream(file));
        }
    }


}
