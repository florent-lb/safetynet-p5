package com.safetynet.alerts.unit;

import com.safetynet.alerts.contract.FireStationManager;
import com.safetynet.alerts.entity.impl.FireStation;
import com.safetynet.alerts.impl.FireStationManagerImpl;
import com.safetynet.alerts.repository.contract.FireStationRepository;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.*;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import javax.inject.Inject;
import java.util.Optional;

import static org.assertj.core.api.Assertions.*;

@Slf4j
@SpringBootTest
@SpringBootApplication(scanBasePackages = "com.safetynet.alerts")
public class FireStationManagerImplTest
{

    @Inject
    private FireStationManager fireStationManager;

    private FireStation station;

    @MockBean
    private FireStationRepository repository;



    @Test
    @DisplayName("Add Firestation")
    public void addFireStation_whenAddAFireStation_shouldNotThrowException()
    {
        station = new FireStation(1,"FireStation test");
        Throwable thrown = catchThrowable(() -> fireStationManager.addFireStation(station));
        Optional.ofNullable(thrown).ifPresent(Throwable::printStackTrace);
        assertThat(thrown).isNull();
    }

    @Test
    @DisplayName("Add Firestation with null parameter")
    public void addFireStation_whenAddAWithoutNumber_shouldThrowNullPointerException()
    {
        station = new FireStation(10,null);
        Throwable thrown = catchThrowable(() -> fireStationManager.addFireStation(station));
        assertThat(thrown).isInstanceOf(NullPointerException.class);
    }

    @Test
    @DisplayName("Update Firestation who doesn't exist")
    public void updateFireStation_whenUpdateWithFalseNumber_shouldThrowNullPointerException()
    {
        String fakeAddress = "";
        repository.findStationByAddress(fakeAddress);

        station = new FireStation(-1,fakeAddress);
        logger.debug("Station use for test : " + station.toString());
        Throwable thrown = catchThrowable(() -> fireStationManager.updateNumber(station));
        assertThat(thrown).isInstanceOf(NullPointerException.class);
    }

    @Test
    @DisplayName("Delete Firestation who doesn't exist")
    public void deleteFireStation_whenDeleteWithFalseNumber_shouldBeOk()
    {
        station = new FireStation(99,"  ");
        Throwable thrown = catchThrowable(() -> fireStationManager.deleteFireStation(station));
        assertThat(thrown).isNull();
    }
}
