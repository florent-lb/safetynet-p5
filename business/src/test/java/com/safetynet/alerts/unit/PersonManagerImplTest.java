package com.safetynet.alerts.unit;

import com.safetynet.alerts.contract.PersonManager;
import com.safetynet.alerts.entity.impl.MedicalRecord;
import com.safetynet.alerts.entity.impl.Person;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.inject.Inject;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
public class PersonManagerImplTest
{

    @Inject
    private PersonManager personManager;


    @Test
    @DisplayName("Get adults ")
    public void getAdults_whenAnAdultHaveJust18yearsOld_shouldReturnHim()
    {
        String firstName1 = "Jean",lastName1="Dupont",firstName2="Jack",lastName2="Dupont";

        Person p1 = Person.builder()
                .firstName(firstName1)
                .lastName(lastName1).build();

        Person p2 = Person.builder()
                .firstName(firstName2)
                .lastName(lastName2).build();

        MedicalRecord m1 = MedicalRecord.builder()
                .firstName(firstName1)
                .lastName(lastName1)
                .birthdate(LocalDate.now().minusYears(15)).build();

        MedicalRecord m2 = MedicalRecord.builder()
                .firstName(firstName2)
                .lastName(lastName2)
                .birthdate(LocalDate.now().minusYears(18)).build();

        Map<Person, MedicalRecord> myAdults = Map.of(
                p1,m1,p2,m2
        );

       List<Person> adults = personManager.getAdults(myAdults);

       assertThat(adults)
               .isNotEmpty()
               .hasSize(1)
               .contains(p2);


    }


}
