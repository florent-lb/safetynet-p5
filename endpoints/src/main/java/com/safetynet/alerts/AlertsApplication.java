package com.safetynet.alerts;

import com.safetynet.alerts.utils.RepositoryInitializer;
import lombok.extern.log4j.Log4j2;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

@Log4j2(topic = "AlertsApplication")
@SpringBootApplication(scanBasePackages = "com.safetynet.alerts")
public class AlertsApplication {

	public static void main(String[] args) {
		SpringApplication.run(AlertsApplication.class, args);
	}



	@Inject
	private RepositoryInitializer repositoryInitializer;
	/**
	 * Initialisation of the web application
	 * Include initialisation of the repositories
	 */
	@PostConstruct
	private void initialisation()
	{
		logger.info("Loading data...");
		repositoryInitializer.initializeRepositories("data/data.json");

	}
}
