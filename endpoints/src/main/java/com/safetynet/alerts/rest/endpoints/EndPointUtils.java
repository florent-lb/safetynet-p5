package com.safetynet.alerts.rest.endpoints;

import lombok.experimental.UtilityClass;
import org.json.JSONObject;

import java.util.Collection;
import java.util.Map;

@UtilityClass
public class EndPointUtils {

    /**
     * Utility method for convert a Map into JsonObject.toString()
     * @param keys
     * @return Json String representation of the map
     */
    public static String convertToJsonString(Map<String,?> keys)
    {
        JSONObject object = new JSONObject();
        keys.forEach(object::put);
        return object.toString();
    }
    /**
     * Utility method for convert a Map into JsonObject
     * @param keys
     * @return Json representation of the map
     */
    public static JSONObject convertToJsonObject(Map<String,?> keys)
    {
        JSONObject object = new JSONObject();
        keys.forEach(object::put);
        return object;
    }

    public static String convertToJsonString(String key, Collection<?> values)
    {
        JSONObject object = new JSONObject();
        object.put(key,values);
        return object.toString();
    }
}
