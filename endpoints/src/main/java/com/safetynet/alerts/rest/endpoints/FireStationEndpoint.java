package com.safetynet.alerts.rest.endpoints;

import com.safetynet.alerts.contract.ManagerFactory;
import com.safetynet.alerts.entity.impl.FireStation;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.util.*;
import java.util.stream.Collectors;

@RestController
@Slf4j
public class FireStationEndpoint {

    @Inject
    private ManagerFactory managerFactory;

    /**
     * Api for add a new Station
     *
     * @param station to add
     */
    @PostMapping(value = "/firestation")
    public void addFirestation(@RequestBody FireStation station) {
        logger.info("POST /firestation : " + station.toString());
        managerFactory.getFireStationManager().addFireStation(station);

    }

    /**
     * For update a number of station who already exist (search based on address)
     *
     * @param station to update
     */
    @PutMapping(value = "/firestation")
    public void updateFireStation(@RequestBody FireStation station) {
        logger.info("PUT /firestation : " + station.toString());
        managerFactory.getFireStationManager().updateNumber(station);
    }

    /**
     * To remove a fire station from the repository (based on address)
     *
     * @param station to delete
     */
    @DeleteMapping(value = "/firestation")
    public void deleteFireStation(@RequestBody FireStation station) {
        logger.info("DELETE /firestation : " + station.toString());
        managerFactory.getFireStationManager().deleteFireStation(station);
    }

    /**
     * Return the people cover by the station with the number in parameter
     *
     * @param number of the station
     * @return <ul>Json format with 3 key
     * <li>nb_child : number of child in the cover</li>
     * <li>nb_adults : number of adults in the cover</li>
     * <li>people : array of people in the cover</li>
     * </ul>
     */
    @GetMapping(value = "/firestation", produces = "application/json")
    public String getStationCover(@RequestParam("stationNumber") Integer number) {
        String retour = EndPointUtils.convertToJsonString( managerFactory.getFireStationManager().getCoverStation(number));
        logger.info("GET /firestation : stationNumber : " + number + " \nResponse : " + retour);
        return retour;

    }

    /**
     * Api give all phone number of people under cover of the station number in parameter
     *
     * @param number of the fire station
     * @return Json Object with key : phones (array of phone's number)
     */
    @GetMapping(value = "/phoneAlert", produces = "application/json")
    public String getPhoneNumbersOfStationCover(@RequestParam("fireStation") Integer number) {

        String retour =  EndPointUtils.convertToJsonString("phones",
                managerFactory.getFireStationManager()
                        .getPhoneCoverFromStation(number));

        logger.info("GET /phoneAlert : fireStation : " + number + " \nResponse : " + retour);
        return retour;
    }

    /**
     * Api give all homes served by the fire station numbers in parameter
     *
     * @param stationNumbers numbers of stations asked
     * @return Json object with one key per number, each station number's key contain these keys :
     * <ul>
     *     <li>allergies : array</li>
     *     <li>phone : string</li>
     *     <li>medications : array</li>
     *     <li>name : string</li>
     *     <li>age : int</li>
     * </ul>
     */
    @GetMapping(value = "/flood/stations", produces = "application/json")
    public String getStationsCover(@RequestParam("stations") List<Integer> stationNumbers)
    {
        Map<String, Collection<?>> stationCoverMap = new HashMap<>();
        managerFactory.getFireStationManager().getCoverStation(stationNumbers)
                .forEach((key, value) -> {
                    //for each station
                    List<JSONObject> peopleOnStation = value.stream().map(EndPointUtils::convertToJsonObject).collect(Collectors.toList());
                    stationCoverMap.put(key.toString(),peopleOnStation);
                });
        String retour =  EndPointUtils.convertToJsonString(stationCoverMap);
        logger.info("GET /phoneAlert : fireStation : " + Arrays.toString(stationNumbers.toArray()) + " \nResponse : " + retour);
        return retour;

    }
}
