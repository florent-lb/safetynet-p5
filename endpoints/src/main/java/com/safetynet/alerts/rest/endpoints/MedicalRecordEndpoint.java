package com.safetynet.alerts.rest.endpoints;

import com.safetynet.alerts.contract.ManagerFactory;
import com.safetynet.alerts.entity.impl.MedicalRecord;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.inject.Inject;

@RestController
@Slf4j
public class MedicalRecordEndpoint
{
    @Inject
    private ManagerFactory managerFactory;

    /**
     * Api for add a new medical record
     * @param medRecord to add
     */
    @PostMapping(value = "/medicalRecord")
    public void addMedicalRecord(@RequestBody MedicalRecord medRecord)
    {
        logger.info("POST /medicalRecord : medRecord : " + medRecord.toString());
        managerFactory.getMedicalRecordManager().addMedicalRecord(medRecord);
    }

    /**
     * Api for update a medical record (search based on firstName & lastName)
     * @param medRecord to update
     */
    @PutMapping(value = "/medicalRecord")
    public void updateMedicalReccord(@RequestBody MedicalRecord medRecord)
    {
        logger.info("PUT /medicalRecord : medRecord : " + medRecord.toString());
       try
       {
           managerFactory.getMedicalRecordManager().updateMedicalRecord(medRecord);
       }
       catch (NullPointerException ex)
       {
           throw new ResponseStatusException(HttpStatus.NOT_FOUND,ex.getMessage());
       }
    }

    /**
     * Api for delete a medical record (search based on firstName & lastName)
     * @param lastName
     * @param firstName
     */
    @DeleteMapping(value = "/medicalRecord")
    public void deleteMedicalRecord(@RequestParam("lastName") String lastName,@RequestParam("firstName") String firstName)
    {
        logger.info("DELETE /medicalRecord : lastName : " + lastName + " | firstName : " + firstName);
        managerFactory.getMedicalRecordManager().deleteMedicalRecord(lastName,firstName);
    }

}
