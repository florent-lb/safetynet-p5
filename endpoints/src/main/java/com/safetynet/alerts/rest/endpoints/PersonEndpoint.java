package com.safetynet.alerts.rest.endpoints;

import com.safetynet.alerts.contract.ManagerFactory;
import com.safetynet.alerts.entity.impl.Person;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@Slf4j
public class PersonEndpoint {
    @Inject
    private ManagerFactory managerFactory;

    @PostMapping(value = "/person")
    public void addPerson(@RequestBody Person person) {
        logger.info("POST /person : " + person.toString());
        managerFactory.getPersonManager().addPerson(person);
    }

    /**
     * Api for change person information with the key firstName/lastName
     *
     * @param person
     */
    @PutMapping(value = "/person")
    public void updatePerson(@RequestBody Person person) {
        logger.info("PUT /person : " + person.toString());
       try
       {
           managerFactory.getPersonManager().updatePerson(person);
       }
       catch (NullPointerException ex)
       {
           throw new ResponseStatusException(HttpStatus.NOT_FOUND,ex.getMessage());
       }
    }

    /**
     * Api for delete a person from the repository
     *
     * @param lastName
     * @param firstName
     */
    @DeleteMapping(value = "/person")
    public void deletePerson(@RequestParam("lastName") String lastName, @RequestParam("firstName") String firstName) {
        logger.info("DELETE /person : lastName : " + lastName + " | firstName : " + firstName);
        managerFactory.getPersonManager().deletePerson(lastName, firstName);
    }

    /**
     * Api for return people at address in parameter separate in two groups children/people
     *
     * @param address to search
     * @return Json Object with two key children and others
     * each key contains sub keys :
     * <ul>
     *     <li>zip : string</li>
     *     <li>lastName : string</li>
     *     <li>firstName : string</li>
     *     <li>address : string</li>
     *     <li>phone : string</li>
     *     <li>city : string</li>
     *     <li>email : string</li>
     * </ul>
     */
    @GetMapping(value = "/childAlert", produces = "application/json")
    public String childAlert(@RequestParam("address") String address) {

        String retour = EndPointUtils.convertToJsonString(managerFactory.getPersonManager().getChildInformationByAddress(address));
        logger.info("GET /childAlert : address : " + address + " \nResponse : " + retour);
        return retour;
    }

    /**
     * Api return all people at address in parameter with medical record information and station
     *
     * @param address to search
     * @return Json Object with key people (Array of Json Object)
     * each Json Object in array have these keys :
     * <ul>
     *     <li>allergies : array string</li>
     *     <li>phone : string</li>
     *     <li>medications : array string</li>
     *     <li>name : string</li>
     *     <li>station : int</li>
     *     <li>age : int</li>
     * </ul>
     */
    @GetMapping(value = "/fire", produces = "application/json")
    public String fireAlarm(@RequestParam("address") String address) {
        List<JSONObject> peopleRecords = managerFactory.getPersonManager().getPersonInformationsAtAddress(address)
                .stream()
                .map(EndPointUtils::convertToJsonObject)
                .collect(Collectors.toList());

        String retour = EndPointUtils.convertToJsonString("people", peopleRecords);
        logger.info("GET /fire : address : " + address + " \nResponse : " + retour);
        return retour;
    }

    /**
     * Api for have all information on the people with the last name in parameter
     *
     * @param firstName
     * @param lastName
     * @return Json Object with key people (array of json object), each sub node have these keys :
     * <ul>
     *     <li>allergies : array string</li>
     *     <li>zip : string</li>
     *     <li>address : string</li>
     *     <li>phone : string</li>
     *     <li>city string</li>
     *     <li>medications : array string</li>
     *     <li>name : string</li>
     *     <li>age : int</li>
     *     <li>email : string</li>
     * </ul>
     */
    @GetMapping(value = "/personInfo", produces = "application/json")
    public String personInfo(@RequestParam("firstName") String firstName, @RequestParam("lastName") String lastName) {
        List<JSONObject> peopleRecords = managerFactory.getPersonManager().getPersonInformations(firstName, lastName)
                .stream()
                .map(EndPointUtils::convertToJsonObject)
                .collect(Collectors.toList());

        String retour = EndPointUtils.convertToJsonString("people", peopleRecords);
        logger.info("GET /personInfo : firstName : " + firstName + " | lastName : " + lastName + "\nResponse : " + retour);
        return retour;
    }

    /**
     * Api to get all email in the city
     *
     * @param city search
     * @return Json object with key emails (Array of strings)
     */
    @GetMapping(value = "/communityEmail", produces = "application/json")
    public String communityEmail(@RequestParam("city") String city) {
        String retour =  EndPointUtils.convertToJsonString("emails", managerFactory.getPersonManager().getPersonMailFromCity(city));
        logger.info("GET /fire : city : " + city + " \nResponse : " + retour);
        return retour;
    }
}
