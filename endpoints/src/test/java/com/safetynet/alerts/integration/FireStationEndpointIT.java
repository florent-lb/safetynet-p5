package com.safetynet.alerts.integration;

import com.safetynet.alerts.contract.FireStationManager;
import com.safetynet.alerts.entity.impl.FireStation;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;

import javax.inject.Inject;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.Assertions.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Slf4j
public class FireStationEndpointIT {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Inject
    private FireStationManager manager;

    private static Stream<String> createListsStation() {
        return Stream.of("1,2,3","3");
    }

    @Test
    @DisplayName("Add a fire Station")
    public void addFireStation_whenAddFireStation_shouldReturnOk() {
        String url = "http://localhost:" + port + "/firestation";
        HttpEntity<String> request = prepareStationToHttpEntityJson(1, "25 Rue du pont 37000 Tours");

        assertThat(restTemplate.postForEntity(url, request, String.class).getStatusCodeValue()).isEqualTo(200);

        assertThat(manager.getStationByAddress("25 Rue du pont 37000 Tours"))
                .isNotEmpty();

    }

    @Test
    @DisplayName("Update a fire Station")
    public void updateFireStation_whenChangeAddress_ShouldReturnOk() {
        String url = "http://localhost:" + port + "/firestation";

        int newStationNumber = 2;
        HttpEntity<String> request = prepareStationToHttpEntityJson(newStationNumber, "1509 Culver St");

        Throwable throwable = catchThrowable(() -> restTemplate.put(url, request));
        assertThat(throwable).isNull();

        assertThat(manager.getStationByAddress("1509 Culver St"))
                .map(FireStation::getStation)
                .isPresent()
                .get()
                .isEqualTo(newStationNumber);
    }


    @Test
    @DisplayName("Delete a fire Station")
    public void deleteFireStation_whenDeleteStationInTowings_ShouldReturnOkAndDeleteIt() {
        String url = "http://localhost:" + port + "/firestation";

        String addressOfStationToDelete = "748 Townings Dr";

        Throwable throwable = catchThrowable(() ->
        {
            HttpEntity<String> entityToDelete = prepareStationToHttpEntityJson(3, addressOfStationToDelete);
            restTemplate.exchange(url, HttpMethod.DELETE, entityToDelete, Void.class);
        });
        assertThat(throwable).isNull();

        assertThat(manager.getStationByAddress(addressOfStationToDelete))
                .isNotPresent();


    }

    @Test
    @DisplayName("Get Station Cover")
    public void getStationCover_whenDeleteStationInTowings_ShouldReturnOkAndDeleteIt() {

        String url = "http://localhost:" + port + "/firestation?stationNumber=3";

        JSONObject retour = new JSONObject(restTemplate.getForObject(url, String.class));

        assertThat(retour).isNotNull()
                .extracting(jsonObject -> jsonObject.keySet().size())
                .isEqualTo(3);

    }

    @Test
    @DisplayName("Phone alert testing")
    public void phoneAlert_whenSendAPhoneAlert_ShouldReturnOkAndDeleteIt() {

        String url = "http://localhost:" + port + "/phoneAlert?fireStation=3";

        JSONObject retour = new JSONObject(restTemplate.getForObject(url, String.class));

        assertThat(retour).isNotNull();

        assertThat(retour.getJSONArray("phones")
                .toList()
                .stream()
                .map(Object::toString)
                .collect(toList()))
                .isNotEmpty();
    }

    @ParameterizedTest(name = "Station flood testing")
    @MethodSource("createListsStation")
    @DisplayName("Station flood testing")
    public void stationsFlood_whenSendFlood_ShouldReturnOkWithInformationOfPeopleAtTheAddress(String stations) {

        String url = "http://localhost:" + port + "/flood/stations?stations="+stations;
        List<Integer> stationsAsked = Arrays.stream(stations.split(",")).map(Integer::parseInt).collect(toList());;

        JSONObject retour = new JSONObject(restTemplate.getForObject(url, String.class));

        assertThat(retour).isNotNull();

        assertThat(retour.keySet()).size().isEqualTo(stationsAsked.size());

       JSONArray home = retour.getJSONArray(stationsAsked.get(0).toString());

        assertThat(  home.getJSONObject(0).keySet())
                .contains("allergies","phone","medications","name","age");


    }


    private HttpEntity<String> prepareStationToHttpEntityJson(int stationNumber, String address) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        JSONObject station = new JSONObject();
        station.put("station", stationNumber);
        station.put("address", address);
        return new HttpEntity<>(station.toString(), headers);
    }


}
