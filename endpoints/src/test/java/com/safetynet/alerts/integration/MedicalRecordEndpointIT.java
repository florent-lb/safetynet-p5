package com.safetynet.alerts.integration;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.safetynet.alerts.entity.impl.MedicalRecord;
import com.safetynet.alerts.entity.impl.Person;
import com.safetynet.alerts.repository.contract.MedicalRecordRepository;
import com.safetynet.alerts.repository.impl.MedicalRecordRepositoryImpl;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;

import javax.inject.Inject;
import javax.inject.Named;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import static org.assertj.core.api.Assertions.*;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class MedicalRecordEndpointIT {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Inject
    private MedicalRecordRepository recordRepository;

    @BeforeAll
    public static void setup() {
    }

    @Test
    @DisplayName("Add a MedicalRecord")
    public void addMedicalRecord_whenAddMedicalRecord_shouldReturnOk() throws JSONException {
        String url = "http://localhost:" + port + "/medicalRecord";

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        JSONObject personObject = new JSONObject();
        personObject.put("firstName", "Geralt");
        personObject.put("lastName", "of Rivia");
        personObject.put("birthdate", "11/02/1006");
        personObject.put("medications", new JSONArray("[\"aznol:350mg\", \"hydrapermazol:100mg\"]"));
        personObject.put("allergies", new JSONArray("[\"nillacilan\"]"));

        HttpEntity<String> request = new HttpEntity<>(personObject.toString(), headers);
        assertThat(restTemplate.postForEntity(url, request, String.class).getStatusCodeValue()).isEqualTo(200);
    }

    @Test
    @DisplayName("Update a MedicalRecord")
    public void update_whenUpdateReccord_shouldReturnOkWithNewValueInRepository() throws JSONException, JsonProcessingException {
        String url = "http://localhost:" + port + "/medicalRecord";

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        ObjectMapper mapper = new JsonMapper();

        MedicalRecord record = mapper.readValue("{ \"firstName\":\"John\", \"lastName\":\"Boyd\", \"birthdate\":\"03/06/1984\", \"medications\":[\"aznol:350mg\", \"hydrapermazol:100mg\"], \"allergies\":[\"nillacilan\"] }"
                , MedicalRecord.class);
        LocalDate newDate = LocalDate.of(2000, 1, 1);
        record.setBirthdate(newDate);

        JSONObject recordJson = new JSONObject(record);
        recordJson.put("birthdate",newDate.format(DateTimeFormatter.ofPattern("MM/dd/yyyy")));


        HttpEntity<String> request = new HttpEntity<>(recordJson.toString(), headers);
        restTemplate.put(url, request, String.class);
        assertThat(recordRepository.searchMedicalRecordByKey(record.getLastName(), record.getFirstName()))
                .isPresent()
                .map(MedicalRecord::getBirthdate)
                .get()
                .isEqualTo(record.getBirthdate());

    }

    @Test
    @DisplayName("Update an unknown MedicalRecord ")
    public void updatePerson_whenUpdateAUnknownPersonInformation_shouldReturn404() throws JSONException {
        String url = "http://localhost:" + port + "/medicalRecord";

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        MedicalRecord unknownRecord = new MedicalRecord();

        HttpEntity<String> request = new HttpEntity<>(new JSONObject(unknownRecord).toString(), headers);

        assertThat( restTemplate.exchange(url, HttpMethod.PUT, request,Void.class)).extracting(ResponseEntity::getStatusCodeValue).isEqualTo(404);

    }
    @Test
    @DisplayName("Delete a Medical Record")
    public void deleteMedicalRecord_whenDeleteJacob_shouldReturnOkAndJacobMissing() throws JSONException
    {
        String lastName = "Boyd";
        String firstName = "Jacob";
        String url = "http://localhost:" + port + "/medicalRecord?lastName=" + lastName +"&firstName="+firstName;

        restTemplate.delete(url);
        assertThat(recordRepository.searchMedicalRecordByKey(lastName, firstName))
                .isNotPresent();

    }


}
