package com.safetynet.alerts.integration;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.safetynet.alerts.entity.impl.Person;
import com.safetynet.alerts.repository.contract.PersonRepository;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;

import javax.inject.Inject;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

import static org.assertj.core.api.Assertions.*;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Slf4j
public class PersonEndpointIT {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Inject
    private PersonRepository repository;

    @BeforeAll
    public static void setup() {
    }


    @Test
    @DisplayName("Add a Person")
    public void addPerson_whenAddPerson_shouldReturnOk() throws JSONException {
        String url = "http://localhost:" + port + "/person";

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        Person geralt = new Person();
        geralt.setAddress("Skelige");
        geralt.setCity("A big house");
        geralt.setEmail("geralt.thebutcher@sorcerer.spell");
        geralt.setFirstName("Geralt");
        geralt.setLastName("of Rivia");
        geralt.setPhone("666-999-666");
        geralt.setZip("00000000");

        JSONObject personObject = new JSONObject(geralt);

        HttpEntity<String> request = new HttpEntity<>(personObject.toString(), headers);

        assertThat(restTemplate.postForEntity(url, request, String.class).getStatusCodeValue()).isEqualTo(200);
        assertThat(repository.findPerson(geralt.getLastName(), geralt.getFirstName()))
                .isPresent()
                .get().isEqualTo(geralt);
    }

    @Test
    @DisplayName("Update a Person")
    public void updatePerson_whenUpdateAPersonInformation_shouldReturnOk() throws JSONException, JsonProcessingException {
        String url = "http://localhost:" + port + "/person";

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        ObjectMapper mapper = new JsonMapper();
        Person john = mapper.readValue("{ \"firstName\":\"John\", \"lastName\":\"Boyd\", \"address\":\"1509 Culver St\", \"city\":\"Culver\", \"zip\":\"97451\", \"phone\":\"841-874-6512\", \"email\":\"jaboyd@email.com\" }",
                Person.class);

        john.setEmail("newEmail@test.com");

        HttpEntity<String> request = new HttpEntity<>(new JSONObject(john).toString(), headers);
        restTemplate.put(url, request);

        assertThat(repository.findPerson(john.getLastName(), john.getFirstName()))
                .isPresent()
                .map(Person::getEmail)
                .get()
                .isEqualTo(john.getEmail());

    }
    @Test
    @DisplayName("Update an unknown Person ")
    public void updatePerson_whenUpdateAUnknownPersonInformation_shouldReturn404() throws JSONException {
        String url = "http://localhost:" + port + "/person";

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        Person john = new Person();

        HttpEntity<String> request = new HttpEntity<>(new JSONObject(john).toString(), headers);

        assertThat( restTemplate.exchange(url, HttpMethod.PUT, request,Void.class)).extracting(ResponseEntity::getStatusCodeValue).isEqualTo(404);

    }

    @Test
    @DisplayName("Delete a Person")
    public void deletePerson_whenDeleteJacob_shouldReturnOkAndJacobMissing() throws JSONException
    {
        String lastName = "Boyd";
        String firstName = "Jacob";
        String url = "http://localhost:" + port + "/person?lastName=" + lastName +"&firstName="+firstName;

        restTemplate.delete(url);
        assertThat(repository.findPerson(lastName, firstName))
                .isNotPresent();

    }


    @Test
    @DisplayName("Call Childalert")
    public void childAlert_whenAskChildAlertOnAdress_shouldReturnListOfPeople() throws JSONException {
        String url = "http://localhost:" + port + "/childAlert?address=" + "1509 Culver St";

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.TEXT_PLAIN);

        JSONObject personsList = new JSONObject(restTemplate.getForObject(url, String.class));
        assertThat(personsList.get("children")).isNotNull();
    }

    @Test
    @DisplayName("Call personInfo")
    public void personInfo_whenAskInfoPersonBoyd_shouldReturnListOfPeople() throws JSONException {
        String url = "http://localhost:" + port + "/personInfo?lastName=Boyd&firstName=John";


        JSONObject retourJson = new JSONObject((restTemplate.getForObject(url, String.class)));
        JSONArray personsArray = retourJson.getJSONArray("people");

        assertThat(personsArray.length()).isGreaterThan(0);
    }

    @Test
    @DisplayName("Call communityEmail")
    public void communityEmail_whenAskcityCulver_shouldReturnListOfPeopleLiveInCulver() throws JSONException {
        String url = "http://localhost:" + port + "/communityEmail?city=Culver";


        JSONObject retourJson = new JSONObject((restTemplate.getForObject(url, String.class)));
        JSONArray personsArray = retourJson.getJSONArray("emails");

        assertThat(personsArray).isNotEmpty();
    }

    @Test
    @DisplayName("Call fire Alarm")
    public void fireAlarm_whenSendFireAlarm_shouldReturnListOfPeopleLiveInCulver() throws JSONException {
        String paramEncoded = URLEncoder.encode("1509 Culver St", StandardCharsets.UTF_8);
        String url = "http://localhost:" + port + "/fire?address=" + paramEncoded;


        JSONObject retourJson = new JSONObject((restTemplate.getForObject(url, String.class)));
        JSONArray personsArray = retourJson.getJSONArray("people");

        assertThat(personsArray)
                .isNotEmpty();
        assertThat(personsArray.getJSONObject(0).keySet())
                .contains("allergies","phone","medications","name","age","station");

    }

}
