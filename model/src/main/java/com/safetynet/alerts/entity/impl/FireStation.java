package com.safetynet.alerts.entity.impl;

import com.safetynet.alerts.entity.contract.Entity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class FireStation implements Entity
{
    /**
     * Number of the station
     */
    private int station;

    /**
     * Address of the station
     */
    private String address;
}
