package com.safetynet.alerts.entity.impl;

import com.safetynet.alerts.entity.contract.Entity;
import lombok.*;

@Data
@EqualsAndHashCode(of = {"firstName","lastName"},callSuper = false)
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Person implements Comparable<Person>,Entity  {

    private String firstName;

    private String lastName;

    private String address;

    private String city;

    private String zip;

    private String phone;

    private String email;

    @Override
    public int compareTo(Person o) {
       return address.compareTo(o.getAddress());
    }
}
