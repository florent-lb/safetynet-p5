package com.safetynet.alerts.repository.contract;

import com.safetynet.alerts.entity.impl.FireStation;

import java.util.List;
import java.util.Optional;

public interface FireStationRepository extends Repository<FireStation> {
    /**
     * Search a Fire station by its address
     * @param address address search
     * @return Firestation if present
     */
    Optional<FireStation> findStationByAddress(String address);

    /**
     * Search a Fire stations by their number
     * @param number number search
     * @return Firestation if present
     */
    List<FireStation> findStationByStation(int number);

    /**
     * Delete a Fire Station from cache
     * @param station the station to delete
     */
    void deleteFireStation(FireStation station);
}
