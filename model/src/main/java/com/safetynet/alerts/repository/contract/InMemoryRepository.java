package com.safetynet.alerts.repository.contract;

import com.safetynet.alerts.entity.contract.Entity;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.util.*;

/**
 * In memory(RAM) implementation of {@link Repository}
 * @param <T>
 */
@Slf4j
public abstract class InMemoryRepository<T extends Entity> implements Repository<T>
{
    /**
     * Collection represent the repository
     * Default implementation {@link HashSet}
     */
    @Getter(AccessLevel.PROTECTED)
    private Set<T> entities = new HashSet<>();


    @Override
    public void add(T t) {
        logger.debug("Add " + t.getClass().getSimpleName() + " : " + t.toString());
        getEntities().add(t);
    }

    @Override
    public void delete(T t) {
        logger.debug("Remove " + t.getClass().getSimpleName() + " : " + t.toString());
        getEntities().remove(t);
    }

    @Override
    public Set<T> readAll() {
        return Collections.unmodifiableSet(getEntities());
    }


}
