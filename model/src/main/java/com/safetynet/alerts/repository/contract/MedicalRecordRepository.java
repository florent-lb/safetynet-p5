package com.safetynet.alerts.repository.contract;

import com.safetynet.alerts.entity.impl.MedicalRecord;

import java.util.Optional;

public interface MedicalRecordRepository extends Repository<MedicalRecord> {

    /**
     * Used to search a medical record
     * @param lastName last Name on record
     * @param firstName first name on record
     * @return potential Medical record
     */
    Optional<MedicalRecord> searchMedicalRecordByKey(String lastName, String firstName);
    /**
     * Remove and add the record who have the same equals/hashcode
     * @param medRecord record to update
     */
    void updateMedicalReccord(MedicalRecord medRecord);
}
