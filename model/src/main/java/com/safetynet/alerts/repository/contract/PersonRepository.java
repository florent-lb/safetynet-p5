package com.safetynet.alerts.repository.contract;

import com.safetynet.alerts.entity.impl.Person;
import com.safetynet.alerts.repository.contract.Repository;

import java.util.List;
import java.util.Optional;

public interface PersonRepository extends Repository<Person> {
    /**
     * Update a person object (seach based on equals see {@link Person#equals})
     * @param person
     */
    void updatePerson(Person person);
    /**
     * Method to search a person based on lastName & firstName
     * @param lastName
     * @param firstName
     * @return potentially a person
     */
    Optional<Person> findPerson(String lastName, String firstName);
    /**
     * Method to search people based on lastName
     * @param lastName
     * @return a List of people (Person.class)
     */
    List<Person> findPerson(String lastName);
    /**
     * Search people based on their address
     * @param address
     * @return List of people (Person.class)
     */
    List<Person> searchByAddress(String address);
    /**
     * Search people based on their city
     * @param city
     * @return List of people (Person.class)
     */
    List<Person> searchByCity(String city);
}
