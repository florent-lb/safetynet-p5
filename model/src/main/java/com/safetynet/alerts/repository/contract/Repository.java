package com.safetynet.alerts.repository.contract;

import com.safetynet.alerts.entity.contract.Entity;

import java.util.Set;

public interface Repository<T extends Entity>
{
    /**
     * Method for add a new object in repository
     * @param t
     */
    void add(T t);
    /**
     * Method for delete a new object in repository
     * @param t
     */
    void delete(T t);

    /**
     * Return all entities in repository
     * Nota : This set can be unmodifiable
     * @return
     */
    Set<T> readAll();

}
