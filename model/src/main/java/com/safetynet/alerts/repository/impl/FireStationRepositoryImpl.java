package com.safetynet.alerts.repository.impl;

import com.safetynet.alerts.entity.impl.FireStation;
import com.safetynet.alerts.repository.contract.FireStationRepository;
import com.safetynet.alerts.repository.contract.InMemoryRepository;
import lombok.extern.slf4j.Slf4j;

import javax.inject.Named;
import javax.inject.Singleton;
import java.util.*;
import java.util.stream.Collectors;

/**
 * In memory Singleton Repository for FireStations
 */
@Slf4j
@Singleton
@Named
public class FireStationRepositoryImpl extends InMemoryRepository<FireStation> implements FireStationRepository {

    @Override
    public Optional<FireStation> findStationByAddress(String address)
    {
        return getEntities().stream()
                .filter(fireStation -> fireStation.getAddress().equals(address))
                .findFirst();
    }


    @Override
    public List<FireStation> findStationByStation(int number)
    {
        System.out.println("Search Station : " + number);
        logger.debug("Search Station : " + number);
        return getEntities()
                .stream()
                .filter(fireStation -> fireStation.getStation() == (number))
                .collect(Collectors.toList());
    }


    @Override
    public void deleteFireStation(FireStation station)
    {
        findStationByAddress(station.getAddress())
                .ifPresent(this::delete);
    }

}
