package com.safetynet.alerts.repository.impl;

import com.safetynet.alerts.entity.impl.FireStation;
import com.safetynet.alerts.entity.impl.MedicalRecord;
import com.safetynet.alerts.repository.contract.InMemoryRepository;
import com.safetynet.alerts.repository.contract.MedicalRecordRepository;
import lombok.Getter;
import lombok.extern.log4j.Log4j2;

import javax.inject.Named;
import javax.inject.Scope;
import javax.inject.Singleton;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

/**
 * In memory Singleton Repository for Medical Records
 */
@Log4j2(topic = "MedicalRecordRepository")
@Singleton
@Named
public class MedicalRecordRepositoryImpl extends InMemoryRepository<MedicalRecord> implements MedicalRecordRepository {

    @Override
    public Optional<MedicalRecord> searchMedicalRecordByKey(String lastName, String firstName) {
        return getEntities().stream()
                .filter(medicalRecord -> medicalRecord.getLastName().equals(lastName))
                .filter(medicalRecord -> medicalRecord.getFirstName().equals(firstName))
                .findFirst();
    }


    @Override
    public void updateMedicalReccord(MedicalRecord medRecord) throws NullPointerException {

        if (searchMedicalRecordByKey(medRecord.getLastName(), medRecord.getFirstName()).isPresent()) {
            delete(medRecord);
            add(medRecord);
        }
        else
        {
            throw new NullPointerException("Update Failed : unkown medicalRecord, add it Before or check your data");
        }
    }

}
