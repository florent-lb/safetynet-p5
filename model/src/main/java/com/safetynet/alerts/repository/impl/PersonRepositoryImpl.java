package com.safetynet.alerts.repository.impl;

import com.safetynet.alerts.entity.impl.FireStation;
import com.safetynet.alerts.entity.impl.Person;
import com.safetynet.alerts.repository.contract.InMemoryRepository;
import com.safetynet.alerts.repository.contract.PersonRepository;
import lombok.extern.log4j.Log4j2;

import javax.inject.Named;
import javax.inject.Singleton;
import java.util.*;
import java.util.stream.Collectors;

/**
 * In memory Singleton Repository for Person
 */
@Log4j2(topic = "PersonRepository")
@Singleton
@Named
public class PersonRepositoryImpl extends InMemoryRepository<Person> implements PersonRepository {

    @Override
    public void updatePerson(Person person) throws NullPointerException {
       if(getEntities().stream()
                .anyMatch(myPerson -> myPerson.equals(person)))
       {
           getEntities().remove(person);
           getEntities().add(person);
       }
       else
       {
           throw new NullPointerException("This person doesn't exist ! add it before or check your data");
       }
    }


    @Override
    public Optional<Person> findPerson(String lastName, String firstName) {

      return getEntities().stream()
                .filter(person -> person.getFirstName().equals(firstName))
                .filter(person -> person.getLastName().equals(lastName))
                .findFirst();

    }


    @Override
    public List<Person> findPerson(String lastName) {

        return getEntities().stream()
                .filter(person -> person.getLastName().equals(lastName))
                .collect(Collectors.toList());

    }


    @Override
    public List<Person> searchByAddress(String address) {
        return getEntities().stream()
                .filter(person -> person.getAddress().equals(address))
                .collect(Collectors.toList());
    }

    @Override
    public List<Person> searchByCity(String city) {
        return getEntities().stream()
                .filter(person -> person.getCity().equals(city))
                .collect(Collectors.toList());
    }

  }
