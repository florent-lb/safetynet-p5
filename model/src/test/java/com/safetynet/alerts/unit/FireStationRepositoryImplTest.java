package com.safetynet.alerts.unit;

import com.safetynet.alerts.entity.impl.FireStation;
import com.safetynet.alerts.repository.contract.FireStationRepository;
import com.safetynet.alerts.repository.impl.FireStationRepositoryImpl;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.*;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

@Slf4j
public class FireStationRepositoryImplTest {

    private  FireStationRepository repository;

    private FireStation stationSearch;

    @BeforeEach
    public void setup() {
        stationSearch = new FireStation(2, "FireStation Test 2 !");
        repository = new FireStationRepositoryImpl();
        repository.add(stationSearch);
    }


    @Test
    @DisplayName("Add FireStation to repository")
    public void addFireStation_whenAddAFireStation_ShouldNotThrowException() {
        FireStation station = new FireStation(1, "FireStation Test !");
        Throwable thrown = catchThrowable(() ->
                repository.add(station));
        Optional.ofNullable(thrown).ifPresent(Throwable::printStackTrace);
        assertThat(thrown).isNull();
    }

    @Test
    @DisplayName("Search the FireStation added")
    public void search_whenSearchAFireStation_ShouldReturnOneFireStation()
    {

        System.out.println(Arrays.toString(repository.readAll().toArray()));

        List<FireStation> stations = repository.findStationByStation(2);
        assertThat(stations).isNotNull().isNotEmpty();

    }

    @Test
    @DisplayName("Delete the FireStation added")
    public void delete_whenSearchAFireStationAfterDeleteIt_ShouldReturnNull() {
        repository.deleteFireStation(stationSearch);
        List<FireStation> stations = repository.findStationByStation(1);
        assertThat(stations).isEmpty();
    }
}
