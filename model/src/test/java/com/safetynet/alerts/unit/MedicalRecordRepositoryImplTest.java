package com.safetynet.alerts.unit;

import com.safetynet.alerts.entity.impl.MedicalRecord;
import com.safetynet.alerts.entity.impl.Person;
import com.safetynet.alerts.repository.contract.MedicalRecordRepository;
import com.safetynet.alerts.repository.impl.MedicalRecordRepositoryImpl;
import com.safetynet.alerts.repository.impl.PersonRepositoryImpl;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

public class MedicalRecordRepositoryImplTest
{
    private static MedicalRecordRepository repository;

    @BeforeAll
    public static void setup()
    {
        repository = new MedicalRecordRepositoryImpl();
    }
    @Test
    @DisplayName("When try to update a medical record who doesn't exist")
    public void updatePerson_whenPersonDoNotExist_ShouldThrowNullPointer()
    {
        MedicalRecord unknowMD = new MedicalRecord();

        Throwable thrown = catchThrowable(() -> repository.updateMedicalReccord(unknowMD));

        assertThat(thrown).isInstanceOf(NullPointerException.class);

    }
}
