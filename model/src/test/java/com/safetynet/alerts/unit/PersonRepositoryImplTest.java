package com.safetynet.alerts.unit;

import com.safetynet.alerts.entity.impl.Person;
import com.safetynet.alerts.repository.impl.PersonRepositoryImpl;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

public class PersonRepositoryImplTest
{
    private static PersonRepositoryImpl repository;

    @BeforeAll
    public static void setup()
    {
        repository = new PersonRepositoryImpl();
    }
    @Test
    @DisplayName("When try to update a people who doesn't exist")
    public void updatePerson_whenPersonDoNotExist_ShouldThrowNullPointer()
    {
        Person unknowPerson = new Person();

        Throwable thrown = catchThrowable(() -> repository.updatePerson(unknowPerson));

        assertThat(thrown).isInstanceOf(NullPointerException.class);

    }
}
